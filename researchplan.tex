\documentclass[a4paper,notitlepage,10pt]{article}

\usepackage[backend=biber,
style=authoryear,
citestyle=authoryear,
sorting=nyt,
url=false,
isbn=false]{biblatex}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage[marginal]{footmisc}
\usepackage{hyperref}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{todonotes}

\hypersetup{colorlinks=true,linkcolor=blue,citecolor=blue,filecolor=blue,urlcolor=blue}

\addbibresource{bib.bib}
\AtEveryBibitem{\clearlist{language}}
\AtEveryBibitem{\clearname{editor}}
\AtEveryBibitem{\clearfield{title}}
%\AtEveryBibitem{\clearfield{eprint}}
\renewbibmacro{finentry}{%
  \finentry\addspace
  \printfield{annote}%
  }
\renewbibmacro{in:}{}
\DeclareSortingScheme{noneyear}{
 \sort{\citeorder}
 \sort{\field{year}}
}

\begin{document}
\title{\bfseries Research Plan}
\author{Jack Yates\vspace{2ex}\\Supervisors: Prof. Paul Palmer, Dr. Beth Biller and Prof. Charles Cockell}
\date{\today}
\maketitle

\renewcommand{\labelitemi}{$\diamond$}
\renewcommand{\labelitemii}{$\circ$}
\renewcommand{\labelitemiii}{\raise.47ex\hbox{\footnotesize$\rhd$}}

\listoftodos

\section{Context}

\todo{Describe some unanswered questions in each area that may be addressed in the project.}

\subsection{Exoplanets}

\subsubsection{Background}

Exoplanets are planets that orbit stars other than the Sun. The first exoplanet was discovered in 1992 \parencite{Wolszczan1992}, and in total 1853 exoplanets have been discovered to date\footnote{See \url{http://exoplanets.eu} for an up-to-date catalogue.}. Almost 1000 of these exoplanets were discovered by a NASA space observatory called the Kepler Space Telescope\footnote{See \url{http://www.nasa.gov/mission_pages/kepler/main/} for up-to-date information}. In addition, Kepler has discovered around 4200 planetary candidates, most of which are probably real planets \parencite{Fressin2013}. \autoref{noexoplanets} shows the number of exoplanets discovered to date by year and method of their discovery. 

The discovered planets exhibit a very large range of properties. Planets may be categorised by size; there are small, Earth-size planets (with $R \leq 1.25 R_\oplus$, where $R$ is the planet's radius and $R_\oplus$ is 6371~km, the Earth's radius), large terrestrial planets called super-Earths and small gas planets called mini-Neptunes (both with $1.25 R_\oplus \leq R \leq 2 R_\oplus$), `Neptunes' ($2R_\oplus \leq R \leq 6 R_\oplus$), `Jupiters' ($6 R_\oplus \leq R \leq 15 R_\oplus$) and huge gas giants many times larger than Jupiter ($15 R_\oplus < R$). Planets exhibit a range of effective temperatures and orbital separations, from the `hot Jupiters,' which orbit at very small orbital separations with effective temperatures\footnote{The temperature of the planet, assuming it emits all radiation with a black-body spectrum.} of thousands of Kelvin, to planets at thousands of AU\footnote{1 AU (astronomical unit) is equivalent to the semi-major axis of the Earth's orbit - approximately the distance between the Sun and the Earth, or 150 million km.} which are essentially self-heated. \textcite{Batalha2014,Buchhave2014,Spiegel2014} provide recent discussions on exoplanet populations.

\begin{figure}
\begin{center}
\includegraphics[width=0.95\linewidth]{figures/counts.pdf}
\caption{\label{noexoplanets}Number of exoplanets discovered per year, and the method by which they were discovered. Kepler began its operations in 2009 and its enormous success can clearly be seen in the ever-increasing number of planets detected by the transit method.}
\end{center}
\end{figure}

\subsubsection{Exoplanet Detection}

\todo{Background: detection of exoplanets}

\subsubsection{Exoplanet Atmospheres}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\linewidth]{figures/lightcurve.jpg}
\caption{\label{lightcurve} An example of a light curve. Flux (amount of light received at the detector) is plotted against time. As the planet moves onto, across and off the star's disc, the amount light received diminishes, flattens off and then increases again. This light curve was obtained with the TRAPPIST instrument on 21 May 2010. Each black point represents one exposure, and the red line is the fitted light curve, with which we can estimate the planet's properties. The planet that has been detected here is WASP-19b, a hot Jupiter somewhat larger than Jupiter. Water has been detected in WASP-19b's atmosphere. Figure adapted from \href{http://www.eso.org/public/images/eso1023f/}{TRAPPIST/M. Gillon/ESO}.}
\end{center}
\end{figure}

\begin{figure}
\center
\includegraphics[width=0.9\linewidth]{figures/transmissionspec.pdf}
\caption{\label{transspec} An illustration of the principle behind transmission spectroscopy. Viewed at different wavelengths, the planetary atmosphere can look optically thin or thick, depending on the absorption spectrum of the atmosphere. Usually the planet is approximated as a solid disk slightly larger than the real planet to account for absorption in the atmosphere. Here, for each ``star,'' the solid disk on the left (the approximation to the planet) blocks approximately the same fraction of light as the diffuse disk on the right (a more realistic representation of the planet).}
\end{figure}

In the last decade, improvements in telescope and detector technology have allowed us to spectroscopically detect gases, clouds, hazes and other features in a planet's atmosphere. This is usually achieved through transmission spectroscopy, emission spectroscopy or direct imaging. 

Transmission spectroscopy probes the upper layers of a planet's atmosphere whilst the planet transits its host star \parencite{Seager2000}. A spectroscopic observation of the transit produces a number of light-curves, each of a different wavelength. At wavelengths where there is significant absorption in the planetary atmosphere, less light from the star will reach the detector, resulting in a deep transit. Conversely, at wavelengths where the atmosphere is highly transparent, the light curve will be shallow, as most of the light is transmitted straight through the atmosphere. Comparing light curves from different wavelengths allows one to infer which parts of the spectrum have been absorbed in the atmosphere. Using this absorption spectrum we can infer the presence of gases, clouds, particulates or any other phenomena with a spectral signal. A sample light curve is shown in \autoref{lightcurve}. The principle behind transmission spectroscopy is illustrated in \autoref{transspec}. Two light curves are shown in \autoref{transspecdiff}.

Emission spectroscopy is similar to transmission spectroscopy. Rather than observe the light transmitted through the planetary atmosphere during the primary transit, we observe the combination of thermal emissions and reflected light from the planet during the secondary transit\footnote{That is, when the planet passes behind its host star (as opposed to in front of it).}. By observing the secondary transit spectroscopically, we can measure the spectral energy distribution (SED) of the planet's emissions. We can then use the SED to determine which gases are present in the atmosphere. 

\begin{figure}
\begin{center}
\begin{subfigure}[t]{0.45\linewidth}
\includegraphics[width=\linewidth]{figures/transitdiff.pdf}
\caption{\label{transspecdiff} An example of the principle behind transmission spectroscopy. Observing a transit in two wavelengths gives two different light curves. These simulated light curves indicate that the optical depth of the planetary atmosphere is greater at 1.125 $\mu$m than at 1.25 $\mu$m -- that is, more light is absorbed at 1.125 $\mu$m.}
\end{subfigure}
~~~
\begin{subfigure}[t]{0.45\linewidth}
\includegraphics[width=\linewidth]{figures/directimage2.jpg}
\caption{\label{directimg}The first direct image of an exoplanet (2M1207b). The planet is the dimmer object depicted in red. 2M1207b is a few times larger than Jupiter and orbits its host star at a distance of approximately 55 AU. Spectral observations have revealed the presence of water in its atmosphere. Image credit: \href{http://www.eso.org/public/images/26a_big-vlt/}{ESO}.}
\end{subfigure}
\end{center}
\end{figure}

Direct imaging is conceptually easier. We observe the light emitted directly from the planet. The difficulty in directly imaging a planet is separating the light from the planet and the light from the host star; the star is usually over a million times brighter than the planet. A frequently-used analogy is that detecting an Earth-like planet through direct imaging is as difficult as detecting a firefly next to a searchlight in New York using a telescope in California. Complicated optical set-ups are required to reduce the contrast between the star and the planet far enough for the observations to be useful. See \textcite{Duchene2008,Oppenheimer2009,Kalas2011,Hinkley2012,Males2014} for reviews of the difficulties, techniques and prospects of direct imaging. \autoref{directimg} show the first direct image of an exoplanet. The planet that has been detected is 2M1207b.

The first detection of an exoplanet atmosphere was achieved with transmission spectroscopy by \textcite{Charbonneau2002}, who detected the sodium doublet in the atmosphere of the planet HD 209458b. Indeed, this method is the most successful to date. Emission spectra of two exoplanets were first detected by \textcite{Deming2005,Charbonneau2005} (at approximately the same time). \textcite{Chauvin2004} were the first to directly detect the emission from an exoplanet. In total, more than 50 exoplanet atmospheres have been measured or in some way detected \parencite{Madhusudhan2014}. For a recent summary of some of the most impressive detections, see \textcite{Burrows2014,Seager2014}. 

\subsubsection{Brown Dwarfs}

Brown dwarfs are not exoplanets; they are essentially small, failed stars. Specifically, brown dwarfs are not massive enough to sustain significant hydrogen fusion in their cores (as opposed to stars, which obviously do fuse hydrogen). Brown dwarfs have significant hydrogen/helium atmospheres and have masses between those of the giant gas planets and about 75 $M_\mathrm{J}$ (Jupiter masses, with $1~M_\mathrm{J}=1.9\times10^{27}~\mathrm{kg}\approx320~M_\oplus$, where $M_\oplus$ is an Earth mass or $6.0\times10^{23}~\mathrm{kg}$), where hydrogen fusion begins. The line between brown dwarfs and large gas giants is not currently well-defined, and several methods have been proposed to distinguish between the two. One common definition is that objects that fuse deuterium (requiring masses greater than about $13~M_\mathrm{J} \approx 4000~M_\oplus$) are brown dwarfs, whilst smaller objects are called gas giants. 

Unlike planets, brown dwarfs can form independently of any star -- a so-called `free-floating' brown dwarf. Indeed, several free-floating brown dwarfs have been detected. (In fact, free-floating brown dwarfs are often easier to detect than companions to stars, as the signal from the brown dwarf is not washed out by an extremely bright star.) As free-floating brown dwarfs are not heated by a nearby star, they tend to be some of the coldest known brown dwarfs. \todo{citations}

As brown dwarfs cannot fuse hydrogen, they gradually cool after formation. Brown dwarfs are classed by features in their spectra, by way of an extension of the OBAFGKM scale that is used for normal stars. There are four classes of brown dwarfs, which are decreasingly hot: class M (which also includes very small stars), class L, class T and class Y (which includes only a small number of brown dwarfs, which are the coolest known). Y dwarfs generally have temperatures below 500 K, and some known Y dwarfs have temperatures lower than 300 K (approximately room temperature). \todo{citations}

Brown dwarfs are considered here for two reasons. Firstly, the process of detecting a brown dwarf (or its atmosphere) is largely the same as the process of detecting an exoplanet (or its atmosphere). Secondly, despite the fact that brown dwarfs lack a true solid surface, it is possible that there are some regions in brown dwarf atmospheres that are habitable. Earth's atmosphere (up to the tropopause) contains a surprising number of microbes, and these microbes are thought to have important roles in cloud formation and may have some responsibility for precipitation formation. It is as yet unclear whether these organisms are simply being transported through the atmosphere or whether they actually spend a significant proportion of their lifecycles there. It is conceivable that similar organisms could live in convective regions of brown dwarf atmospheres, especially the atmospheres of cool Y dwarfs. \todo{citations}

\subsection{Astrobiology}

\subsubsection{Background}

By any reasonable definition, Earth is the only celestial body known to have developed life. Our expectations for the conditions necessary to develop life are therefore derived from observations on biochemistry and the limiting conditions for life on Earth. 

\textcite{McKay2014} lists four requirements for life:
\begin{itemize}
\item the presence of liquid water,
\item the availability of energy,
\item the availability of carbon and
\item the availability of other elements. 
\end{itemize}

Of these, the presence of liquid water is the most important; every organism that has ever been observed requires the liquid water at some point in its lifecycle. From an astronomical perspective, liquid water on a planet is the easiest to rule out. Liquid water exists over a narrow range of temperatures, and a planet's effective temperature can be easily calculated with some basic assumptions. This gives rise to the concept of the `habitable zone' -- the region within which a planet could maintain some liquid water at its surface. As mentioned in the previous section, there may also be `habitable zones' within brown dwarf atmospheres. 

Many alternative types of life have been proposed. This includes alternative biochemistries (for example, life that uses hydrocarbons in place of water and life that uses silicon in place of carbon) and alternative habitats (for example, subsurface oceans such as those on Enceladus, gas giant atmospheres, planetary satellites or moons, bodies with liquid hydrocarbons such as Titan, Venus- or Mars-like planets, so-called `Dune' planets and water planets). These propositions are obviously entirely speculative, as we have no evidence for alternative forms of life. \todo{citations}

With these considerations in mind, warm, Earth-sized planets are the most interesting from an astrobiological standpoint. These planets naturally have the conditions most similar to those of Earth, and therefore seem the most likely to develop life. 

\subsubsection{Biosignatures}

The ability to detect the constituents of a planetary atmosphere opens up the possibility of detecting biosignatures. Biosignatures are indicators of the possible presence of life, and were first considered by \textcite{LOVELOCK1965} and \textcite{LEDERBERG1965} at approximately the same time. These are of particular interest to astronomers, who generally do not need to concern themselves with the specifics of the biology. 

Most proposed biosignatures are evidence of some kind of thermodynamic disequilibrium. For example, redox disequilibrium would indicate that oxygen and reducing gases were being continuously produced on the planet. On Earth, the abundances of oxygen and methane are several orders of magnitude out of chemical equilibrium -- here, at least, this is due to metabolism by life. Other atmospheric biosignatures include CH$_{4}$, N$_{2}$O, CH$_{3}$SH and CFCs\footnote{CFCs would specifically indicate industrialised life.}. The photosynthetic `red edge' in the reflectance spectrum of a planet is a non-atmospheric biosignature. Earth's albedo increases significantly at around 700 nm; this rise in albedo is due to vegetation, which reflects long-wave radiation as a cooling mechanism. Water vapour, though not a biosignature, may suggest that a large volume of water is present on the planet's (or body's) surface. \todo{citations}

With current technology, it is difficult to detect biosignatures; indeed, none have ever been detected (excluding water vapour, which is not a true biosignature). In theory, the next generation of space- and ground-based telescopes should have the precision and stability necessary to detect biosignatures at Earth-like levels. Obviously, the detection of a biosignature would be an extraordinary result -- perhaps the biggest discovery in the history of science. Non-detections (when we expect to have the precision necessary for a detection) will still indicate when, where and how life cannot (or is unlikely to) develop, and give us a better idea of our place in the universe. 

\subsection{Future Technology}\todo{Finish future technology section}

Partly due to the huge success of Kepler and other exoplanet detection campaigns, many exoplanet projects have been proposed, or are now in development. Collectively these projects have already cost many billions of dollars, and will cost many billions more. It is hoped that these instruments will have the sensitivity necessary to detect Earth-sized planets in the habitable zone, or to detect biosignatures. 

In general, these new instrument can be classified into ground- and space-based instruments. 

Ground-based instruments that are being developed are a new class of extremely large telescopes. Including in this category is the European Extremely Large Telescope (E-ELT) with a diameter\footnote{All of these telescopes use segmented mirrors, since single glass mirrors deform at such large sizes. Consequently the given `diameter' is the diameter of a single primary mirror with the equivalent resolving power.} of 39.3 m, the Thirty Metre Telescope (TMT) with a diameter of 30 m and the Giant Magellan Telescope (GMT) with a diameter of 25.5 m. \todo{citations}

Some of these should be sensitive enough to detect Earth-like planets and their atmospheres

Telescopes are already being built, so it behooves us to develop efficient strategies for detecting exoplanets, exoplanet atmospheres and biosignatures. 

\subsection{Significance and Novelty}

Models of exoplanet atmospheres are usually fairly basic; often they use a few molecules of interest in a number of layers with fixed pressures and temperatures. Clouds are modelled in a basic fashion. Conversely, models of Earth's atmosphere are much more developed, including a variety of well-studied effects. \todo{citations}

We investigate habitability and biosignatures according to what we know about Earth, so it is reasonable to expect conditions similar to those on Earth on other habitable planets. Despite this, inter-disciplinary work, including results from the Earth sciences and from astrophysics, seems to be uncommon. 

This project will apply knowledge we have gained by studying Earth to exoplanets. Specifically we will modify and use models of the Earth's atmosphere to investigate exoplanet atmospheres, and try to bridge the gap between the two subjects.

\printbibliography[heading=subbibnumbered]

\pagebreak

\section{Research Aims}

The general aims of the project are to investigate (or investigate some aspects of) the following:

\begin{enumerate} 

\item the detectability of (atmospheric) biosignatures with next-generation telescopes

\item the differences between models of Earth's atmosphere and models of exoplanet atmospheres, and the consequences of the choice of model with regard to predictive power of the results

\item the possibility of novel habitats for life in exoplanet and brown dwarf atmospheres and the constraints of those environments with regards to life

\end{enumerate}

\section{Time Plan}

There are a number of potential avenues of investigation. With a functional model, it will be possible to investigate a variety of questions, depending on what seems appealing or important at the time. Initially, we will be investigating brown dwarf habitability, and it is expected that this `sub-project' will take several months. 

\begin{center}
\begin{tabular}{ccl}
& \bfseries Month & \bfseries Task \\\hline
{\bfseries Research} & 1-9 & Brown dwarf project \\
& 10-18 & (Second research project) \\
& 19-27 & (Third research project) \\
{\bfseries Administration} & 1-9 & Confirmation report \\
& 6-9 & Confirmation panel \\
& 6-9 & Confirmation presentation \\
& 12 & 12-month progress report \\
& 24 & 24-month progress report and thesis timetable \\
& 33 & 33-month progress report \\
{\bfseries Thesis and viva} & 12-33 & Thesis (first draft) \\
& 33-36 & Thesis changes and submission \\
& 36+ & Viva seminar \\
& 36+ & Thesis defence \\
\end{tabular}
\end{center}

\section{Project Resources}

The use of high-performance computing resources will be useful, if not required, throughout the project. This will dramatically speed up model runs, which may take days or weeks on an individual desktop computer. The department's servers and the university's ECDF should be more than sufficient for the vast majority of the project work. The backup strategy for this work is detailed in \autoref{backups}.

Observational data for model verification may be required for each of the following:
\begin{itemize}
\item Earth's atmosphere, 
\item exoplanet atmospheres, 
\item telescopes and instruments,
\item biological growth times, reproduction times, etc.
\end{itemize}
It is as yet unclear whether these data will be necessary, as it will depend on the models to be used in the project. Project supervisors will be able to help acquire these data. 

\section{Data Management Plan}

\subsection{Code}

Any code that is been produced or adapted during the project will need version control, to ensure that changes are tracked and that every version of each program is available for use (for reproducibility). Git and BitBucket will be used for version control. This combination offers an unlimited number of private repositories for free. Code that has been produced by others may need to be stored securely, depending on the conditions of access. DataStore and private repositories should be sufficiently secure for this. The use of personal hard drives, memory sticks, public scratch space and so on will be avoided where security is a concern. Code that has not been produced or adapted during the project will not need version control or secure storage (although it will still be backed up). 

\subsection{Model Outputs}

In addition to the model code, a large number of data files (model outputs) will be produced throughout the project. These will most likely consist of plain text files containing numbers. In addition to the model outputs, there will be statistical data, figures and summaries, which will be derived from the model results. 

Where possible, model outputs should be concentrated into the smallest possible number of files. This will make organisation and data handling easier. Even so, many files will likely be produced. These will be small files and should not require an extreme amount of storage space. DataStore will be used to store these files.

The model outputs will need to be stored in a consistent, sensible manner, so that old results can be accessed if they are required again. Where possible, data files will be named according to the date and time they were produced, a model version number and any other relevant information. A sample file might be called \texttt{modelX\_vY\_YY-MM-DD\_HH-MM-SS.dat} where \texttt{modelX} describes what the results are, \texttt{vY} means version \texttt{Y}, and \texttt{YY-MM-DD\_HH-MM-SS} describes the time when the model run was initiated\footnote{The time of the model run initiation will be more useful here than the time when the file was produced, as it allows data products from a single model run to be grouped together.}. Most data will be simply output into plain text files (\texttt{.dat} files). Files should be saved in folders to make organisation easier. 

Model results should be reproducible. It may be necessary to reproduce data with slightly different initial conditions or a new version of a model. If a bug were to be found in the model that had affected some results, for example, it would be necessary to reproduce those results with an updated model. Therefore, where possible, information about the data contained in these files will be included at the start of the file (in a header). All data in the file should be clearly explained in the file header. For example, each column of tabulated data should have a short description of what the numbers in the column represent, and a unit. In addition, the header should contain information about the model parameters used to produce the data. In short, the header should contain all information necessary to reproduce the data in the file, and all information necessary to apply the results in the future. This should ensure that each data file is useful in its own right and does not require additional files (which may get lost) or explanation (which may not be possible) in order to be used. Any other data required to define a model run will be stored.

It is not important that model data are stored securely. None of the data will contain personal or confidential information. 

\subsection{Physical Data}

In addition to the model outputs, some physical data could be required for model verification. This may include data from telescopes, data from observations of Earth's atmosphere or biological data on microbes and other organisms. This will depend on whether models have been produced during the project or not; models produced by other groups should have already been verified with physical results.

This physical data will be stored as best possible -- it may not be possible to structure and name files that were not produced in this project in a manner that is consistent with data produced during this work without a significant effort.

\subsection{Backups}
\label{backups}

All work will be backed up to DataStore. DataStore is managed by the university and is backed up to tape regularly. The allocated 500 GB of storage space should be adequate for the project. 

Files on my personal machine (provided by the department) will be backed up (automatically) daily at 5 PM. Several days of backups will be maintained on DataStore, as long as there is enough storage space to do so. There will therefore be two levels of redundancy; there will be several days of backups on DataStore, each backed up daily and maintained for several days. Currently, 4 old backups (i.e. 5 days in total) are kept. Since the backups are automated, it does not depend on a fallible user, who might forget to backup important work. 

On the department servers, models will be run from DataStore directories (as there is not enough storage space on the server itself), and when using Eddie, work will be backed up to DataStore as frequently as possible (i.e. after model runs complete, at intermediate stages of model runs, etc.). When work cannot be backed up to DataStore, other managed storage will be used. If managed storage is not available (ECDF allocation is fairly small), the work will have to be stored on unmanaged storage until it can be moved to DataStore. This period of time will be minimised, ensuring that only a few hours of work can possibly be lost. 

\subsection{Long Term Plan}

In line with NERC policy, data and models that we have developed will be made available to anyone who wishes to use them after some period of time (the normal NERC policy is 2 years after the data was collected). As the models will be new or adapted, it is possible that they may be of interest to others in the field. Data produced during the project will probably not be as useful (assuming the models are available), as they can simply be generated again. Data will be made available through NERC data centres and models may be made available through a public repository on GitHub or a similar website. 

Peer-reviewed papers will be made available on the arXiv. This is an online pre-print repository that is freely accessible. Uploading papers to the arXiv is standard practice in astrophysics and many other fields.

\section{Supervisory Arrangements}

Prof. Palmer will advise on models of Earth's atmosphere and (atmospheric) modelling in general, and is the principal supervisor. Dr. Biller will advise on detection limits and exoplanets and their atmospheres. Prof. Cockell will advise on biosignatures and potential habitats for life. 

Meetings will be arranged as and when they are necessary. As supervisors have different areas of expertise and different schedules, it will be most convenient to meet supervisors individually when issues arise. For the forseeable future, I will be meeting with Prof. Palmer on a weekly basis. Every few weeks I will meet with two of the supervisors, depending on what is necessary to advance the project. Meetings will be arranged with all three supervisors every 4--6 weeks.

\end{document}
